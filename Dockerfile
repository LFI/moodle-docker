FROM php:7.2-apache

RUN apt-get update && apt-get upgrade -y
RUN apt-get install git curl gnupg2 -y

ADD root/ /
RUN chmod 777 /tmp && chmod +t /tmp

ADD scripts/node.sh /tmp/scripts/
RUN bash /tmp/scripts/node.sh

ADD scripts/php-extensions.sh /tmp/scripts/
RUN /tmp/scripts/php-extensions.sh

ADD scripts/composer.sh /tmp/scripts
RUN /tmp/scripts/composer.sh

ADD scripts/moodle.sh /tmp/scripts/
RUN /tmp/scripts/moodle.sh

ADD scripts/code-checker.sh /tmp/scripts/
RUN /tmp/scripts/code-checker.sh

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

ADD scripts/moodle-package.sh /tmp/scripts
RUN /tmp/scripts/moodle-package.sh

ADD scripts/package.json /var/www/moodle/package.json