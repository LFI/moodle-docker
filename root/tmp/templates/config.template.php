<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->wwwroot   = "http://localhost:8000";
$CFG->dataroot  = '/var/www/moodledata';
$CFG->admin     = 'admin';
$CFG->directorypermissions = 0777;

$CFG->dbtype = 'mysqli';
$CFG->dblibrary = 'native';

$CFG->allowthemechangeonurl = 1;
$CFG->passwordpolicy = 0;

$CFG->phpunit_dbtype = 'mysqli';
$CFG->phpunit_dbhost = 'mysql';
$CFG->phpunit_dbname = getenv('MYSQL_DATABASE');
$CFG->phpunit_dbuser = 'root';
$CFG->phpunit_dbpass = getenv('MYSQL_ROOT_PASSWORD');
$CFG->phpunit_dataroot  = '/var/www/phpunitdata';
$CFG->phpunit_prefix = 't_';

define('TEST_EXTERNAL_FILES_HTTP_URL', 'http://exttests');


require_once(__DIR__ . '/lib/setup.php');
