<?php

/**
 * Moodle PHP Code Style Checker
 */

require('./vendor/autoload.php');

class cli extends PHP_CodeSniffer_CLI {
    private $report = 'full';
    private $reportFile = null;

    public function __construct()
    {
        $this->errorSeverity = 1;
        $this->warningSeverity = 1;
    }

    public function getCommandLineValues()
    {
        $defaults = array_merge(
            $this->getDefaults(),
            [
                'reports' => [$this->report => $this->reportFile]
            ]
        );
        return $defaults;
    }
}

$files = getFiles($argv[1]);

$phpcs = new PHP_CodeSniffer(1, 0, 'utf-8', false);

$cli = new cli();

$phpcs->setCli($cli);

$cli->errorSeverity = 1;
$cli->warningSeverity = 1;

$phpcs->process($files, './moodle');

$phpcs->reporting->printReport('full', false, $cli->getCommandLineValues(), null);

if($phpcs->reporting->totalErrors + $phpcs->reporting->totalWarnings != 0) {
    exit(1);
}

exit(0);

function getFiles($dir, &$results = array())
{
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);

        if (!is_dir($path)) {
            $info = pathinfo($path);
            if(isset($info['extension']) && $info['extension'] === 'php') {
                $results[] = $path;
            }
        } else if ($value != '.' && $value != '..') {
            getFiles($path, $results);
        }
    }

    return $results;
}

