mkdir /var/www/moodledata
chown www-data /var/www/moodledata
mkdir /var/www/phpunitdata
chown www-data /var/www/phpunitdata

npm install -g grunt-cli

git clone --depth 1 https://github.com/moodle/moodle.git -b MOODLE_34_STABLE  /var/www/moodle

cp /tmp/templates/config.template.php /var/www/moodle/config.php

cd /var/www/moodle


composer install
npm install