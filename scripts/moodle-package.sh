#!/usr/bin/env bash

git clone https://git.rwth-aachen.de/LFI/moodle-package.git /srv/moodle-package
cd /srv/moodle-package
npm install
npm install -g